package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
)

var db *sql.DB

type BankDetail struct {
	IFSC     string
	BankName string
	Branch   string
	Address  string
	City     string
	District string
	State    string
}

type BankDetailsReq struct {
	IFSC string
}

func BankDetailsHandler(w http.ResponseWriter, r *http.Request) {
	x := BankDetailsReq{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&x)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	// log.Println(x)

	var t BankDetail

	err = db.QueryRow(
		"SELECT ifsc, ba.name, branch, address, city, district, state FROM branches, banks ba WHERE ba.id = bank_id AND ifsc = $1",
		x.IFSC).Scan(&t.IFSC, &t.BankName, &t.Branch,
		&t.Address, &t.City, &t.District, &t.State)

	if err != nil {

		if err == sql.ErrNoRows {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}

		log.Println("ERROR: Database Error: ", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	var jData []byte
	jData, err = json.Marshal(t)
	if err != nil {
		log.Println("ERROR: JSON Marshal Error: ", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// log.Println(string(jData))
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

type BranchesReq struct {
	BankName string
	City     string
}

type BranchesRes struct {
	Details []BankDetail
}

func BranchesHandler(w http.ResponseWriter, r *http.Request) {
	x := BranchesReq{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&x)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	// log.Println(x)

	l := make([]BankDetail, 0)
	var rows *sql.Rows

	rows, err = db.Query(
		"SELECT ifsc, ba.name, branch, address, city, district, state FROM branches br, banks ba WHERE ba.id = br.bank_id AND br.city = $1 AND ba.name = $2",
		x.City, x.BankName)
	if err != nil {
		log.Println("ERROR: Database Error: ", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	for rows.Next() {
		var t BankDetail
		err = rows.Scan(&t.IFSC, &t.BankName, &t.Branch,
			&t.Address, &t.City, &t.District, &t.State)
		if err != nil {
			log.Println("ERROR: Database Error: ", err)
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}
		l = append(l, t)
	}

	err = rows.Err()
	if err != nil {
		log.Println("ERROR: Database Error: ", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	var jData []byte
	jData, err = json.Marshal(BranchesRes{l})
	if err != nil {
		log.Println("ERROR: JSON Marshal Error: ", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// log.Println(string(jData))
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

func initDB() error {
	var err error

	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		return err
	}

	// Test the connection to the database
	err = db.Ping()
	if err != nil {
		return err
	}

	return nil
}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Hello World, Health Check is PASS"))
}

func main() {

	log.SetFlags(log.Lshortfile)
	log.Println("About to initialize Database")

	if err := initDB(); err != nil {
		log.Println("Database initialize failed")
		log.Fatal(err)
		return
	}

	http.HandleFunc("/bank-details", BankDetailsHandler)
	http.HandleFunc("/branches", BranchesHandler)
	http.HandleFunc("/", RootHandler)

	port := ":" + os.Getenv("PORT")
	log.Println("Database initialized succesfully. Launching the HTTP Server on ", port)
	log.Fatal(http.ListenAndServe(port, nil))
}
